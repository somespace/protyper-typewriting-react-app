import { v4 as uuidv4 } from "uuid";
import { open } from "node:fs/promises";
import fs from "node:fs/promises";
import bodyParser from "body-parser";
import express from "express";
import ViteExpress from "vite-express";

const app = express();
const dataPath = "./src/server/data";
const generatedPath = "./generated/";

app.use(bodyParser.json());
app.use(express.static("public"));

// helper: read and return lesson lines as array of strings
async function _loadSourceLines(filePath) {
  const content = [];
  let file;
  try {
    file = await open(filePath);
  } catch (error) {
    console.log(error);
    throw new Error("Error occurred during loading the text data");
  }

  for await (const line of file.readLines()) {
    content.push(line);
  }

  return content;
}

// CORS
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*"); // allow all domains
  res.setHeader("Access-Control-Allow-Methods", "GET, POST");
  res.setHeader("Access-Control-Allow-Headers", "Content-Type");

  next();
});

app.get("/basics", async (req, res) => {
  const srcLines = await _loadSourceLines(`${dataPath}/basics.txt`);
  res.status(200).json({ lines: srcLines });
});

app.get("/specchar", async (req, res) => {
  const srcLines = await _loadSourceLines(`${dataPath}/specchar.txt`);
  res.status(200).json({ lines: srcLines });
});

app.get("/jssnippets", async (req, res) => {
  const srcLines = await _loadSourceLines(`${dataPath}/jssnippets.txt`);
  res.status(200).json({ lines: srcLines });
});

app.post("/lessons", async (req, res) => {
  const lessonData = req.body;

  if (!lessonData.lesson || !lessonData.timeToFinish || !lessonData.typos) {
    return res.status(400).json({
      message: "Missing data: Lesson name, time or typos are missing.",
    });
  }

  const newLesson = {
    ...lessonData,
    timestamp: Date.now(),
    id: uuidv4(),
  };
  const lessonsFilePath = `${generatedPath}/lessons.json`;
  await fs
    .access(lessonsFilePath, fs.constants.F_OK)
    .catch(() => fs.writeFile(lessonsFilePath, JSON.stringify([])))
    .catch(() => {
      res.status(500).json({ message: "Internal server error" });
    });

  const lessons = await fs.readFile(lessonsFilePath, "utf8");
  const allLessons = JSON.parse(lessons);

  allLessons.push(newLesson);
  await fs.writeFile(
    `${generatedPath}/lessons.json`,
    JSON.stringify(allLessons),
  );
  res.status(201).json({ message: "Lesson saved." });
});

app.get("/res", async (req, res) => {
  const data = await fs.readFile(`${generatedPath}/lessons.json`, "utf8");
  const resultsParsed = JSON.parse(data);
  return res.status(200).json({ results: resultsParsed });
});

ViteExpress.listen(app, 3000, () =>
  console.log("Server is listening on http://localhost:3000..."),
);
