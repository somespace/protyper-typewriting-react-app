export async function loadLessonText(suffix) {
  const response = await fetch(`http://localhost:3000/${suffix}`);
  const resData = await response.json();

  if (!response.ok) {
    throw new Error("Failed to fetch source text");
  }

  return resData.lines;
}

export async function saveLesson(lesson) {
  const response = await fetch("http://localhost:3000/lessons", {
    method: "POST",
    body: JSON.stringify(lesson),
    headers: {
      "Content-Type": "application/json",
    },
  });

  const resData = await response.json();
  const message = resData.message;

  if (!response.ok) {
    throw new Error(message);
  }
}

export async function fetchResults() {
  const response = await fetch("http://localhost:3000/res");
  const resData = await response.json();
  return resData.results;
}
