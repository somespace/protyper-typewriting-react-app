import { createContext, useState } from "react";

const LessonScoreContext = createContext({
  lesson: "",
  timeToFinish: 0,
  typos: 0,
  finished: false,
  setLesson: () => {},
  setTimeToFinish: () => {},
  setTypos: () => {},
  setFinished: () => {},
  setLessonStats: () => {},
  resetLessonStats: () => {},
});

const lessonStatsInit = {
  lesson: "",
  timeToFinish: 0,
  typos: 0,
  finished: false,
};

export function LessonScoreContextProvider({ children }) {
  const [lessonScore, setLessonScore] = useState(lessonStatsInit);

  function setLessonStats(updatedScore) {
    setLessonScore((prevLessonScore) => {
      return {
        ...prevLessonScore,
        ...updatedScore,
      };
    });
  }

  function resetLessonStats() {
    setLessonScore((prevLessonScore) => {
      return {
        ...prevLessonScore,
        ...lessonStatsInit,
      };
    });
  }

  function setLesson(name) {
    setLessonScore((prevLessonScore) => {
      return {
        ...prevLessonScore,
        lesson: name,
      };
    });
  }

  function setTimeToFinish(ms) {
    setLessonScore((prevLessonScore) => {
      return {
        ...prevLessonScore,
        timeToFinish: ms,
      };
    });
  }

  function setTypos(typoCount) {
    setLessonScore((prevLessonScore) => {
      return {
        ...prevLessonScore,
        typos: typoCount,
      };
    });
  }

  function setFinished(isFinished) {
    setLessonScore((prevLessonScore) => {
      return {
        ...prevLessonScore,
        finished: isFinished,
      };
    });
  }

  const lessonScoreCtx = {
    ...lessonScore,
    setLesson,
    setTimeToFinish,
    setTypos,
    setFinished,
    setLessonStats,
    resetLessonStats,
  };

  return (
    <LessonScoreContext.Provider value={lessonScoreCtx}>
      {children}
    </LessonScoreContext.Provider>
  );
}

export default LessonScoreContext;
