export default function getStats(time, charCount, typos) {
  const minuteDuration = time / 60;
  const bpm = Math.floor((1 / minuteDuration) * charCount);
  const accuracy = parseFloat(
    Number(100 - (typos / charCount) * 100).toFixed(2),
  );

  return {
    bpm,
    accuracy,
  };
}

const LESSONS = {
  basics: "Basics",
  jssnippets: "JS Snippets",
  specchar: "Special Characters",
};

export function getLessonNameById(lessonId) {
  const name = LESSONS[lessonId];
  if (!name) {
    return "Unknown Lesson";
  }
  return name;
}
