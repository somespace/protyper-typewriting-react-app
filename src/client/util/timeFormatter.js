export default function formatTime(seconds) {
  if (seconds < 60) {
    return `${seconds} s`;
  } else {
    const min = Math.floor(seconds / 60);
    const sec = seconds % 60;
    return `${min} m ${sec} s`;
  }
}

export function timestampToLocaleDate(timestamp) {
  const date = new Date(timestamp);
  return date.toLocaleDateString();
}

export function timestampToLocaleTime(timestamp) {
  const date = new Date(timestamp);
  return date.toLocaleTimeString();
}
