import { useContext } from "react";
import LessonScoreContext from "../store/LessonScoreContext";

export default function LessonCart({ id, title, description }) {
  const lessonScoreCtx = useContext(LessonScoreContext);

  function handleSelectLesson() {
    lessonScoreCtx.setLesson(id);
  }

  return (
    <div
      className="px-5 py-5 rounded-lg bg-slate-100 hover:bg-slate-200 select-none 
      cursor-pointer justify-self-stretch"
      onClick={handleSelectLesson}
    >
      <article>
        <h2 className="capitalize font-bold">{title}</h2>
        <p>{description}</p>
      </article>
    </div>
  );
}
