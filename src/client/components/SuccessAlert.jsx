import Alert from "../UI/Alert";

export default function SuccessAlert({ message, shouldBeOpen }) {
  return <Alert type="success" message={message} shouldBeOpen={shouldBeOpen} />;
}
