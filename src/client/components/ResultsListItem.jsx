import { Link } from "react-router-dom";
import { getLessonNameById } from "../util/stats";
import {
  timestampToLocaleDate,
  timestampToLocaleTime,
} from "../util/timeFormatter";

export default function ResultsListItem({ result }) {
  const date = timestampToLocaleDate(result.timestamp);
  const time = timestampToLocaleTime(result.timestamp);
  const name = getLessonNameById(result.lesson);

  return (
    <Link to={`${result.id}`} state={result}>
      <div className="bg-slate-100 hover:bg-slate-200 m-2 p-2 rounded">
        <p>
          <strong>{name}</strong>
        </p>
        <p>
          <time>
            {date} -{time}
          </time>
        </p>
      </div>
    </Link>
  );
}
