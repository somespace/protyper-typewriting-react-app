export default function Header() {
  return (
    <header className="flex items-center justify-center h-[15vh] select-none">
      <h1 className="font-bold capitalize text-3xl tracking-widest">
        <span className="uppercase text-slate-500">Pro</span>
        typer
      </h1>
    </header>
  );
}
