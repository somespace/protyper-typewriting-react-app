import formatTime from "../util/timeFormatter";
import getStats from "../util/stats";
import Button from "../UI/Button";
import { useEffect, useRef } from "react";
import ResultData from "./ResultData";

export default function ResultDialog({
  lesson,
  lessonCharCount,
  timeToFinish,
  typos,
  onSave,
  onClose,
}) {
  const formRef = useRef(null);

  useEffect(() => {
    formRef.current.focus();
  }, []);

  function handleKeyDown(event) {
    // Prevent the default action if the spacebar key is pressed
    if (event.keyCode === 32) {
      event.preventDefault();
    }
  }

  function handleSubmit(event) {
    event.preventDefault();
    onSave();
  }

  return (
    <>
      <h2 className="text-2xl font-bold mt-3 mb-6">Your Score</h2>
      <ResultData result={{ lesson, lessonCharCount, timeToFinish, typos }} />
      <form
        ref={formRef}
        className="mt-6 text-right focus:outline-none"
        method="dialog"
        onKeyDown={handleKeyDown}
        onSubmit={handleSubmit}
        tabIndex={-1}
      >
        <Button className="py-2 px-4 mx-4" type="button" onClick={onClose}>
          Close
        </Button>
        <Button
          className="py-2 px-4 bg-slate-500 text-white rounded-md"
          type="submit"
        >
          Save
        </Button>
      </form>
    </>
  );
}
