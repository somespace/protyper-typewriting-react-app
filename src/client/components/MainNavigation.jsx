import { NavLink } from "react-router-dom";

const linkClasses = "mr-4";

const classesWhenActive = ({ isActive }) => {
  return isActive
    ? `${linkClasses} text-white`
    : `${linkClasses} text-slate-300`;
};

export default function MainNavigation() {
  return (
    <nav
      className="flex items-center justify-center h-[5vh] px-10 
      sm:px-5 bg-slate-700"
    >
      <NavLink to="/" className={classesWhenActive} end>
        Home
      </NavLink>
      <NavLink to="/results" className={classesWhenActive}>
        Results
      </NavLink>
    </nav>
  );
}
