export default function CharBlock({
  character,
  isSelected,
  wasTyped,
  hasError,
}) {
  const selectedLetterClasses = " bg-slate-700 text-white";
  const erroredLetterClasses =
    character === " " ? " bg-red-500" : " text-red-500";
  return (
    <li
      className={`min-w-3 text-center
        ${isSelected && selectedLetterClasses}
        ${hasError && erroredLetterClasses}
        ${wasTyped && !hasError && " text-slate-500"}`}
    >
      {character}
    </li>
  );
}
