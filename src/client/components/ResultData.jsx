import { getLessonNameById } from "../util/stats";
import formatTime from "../util/timeFormatter";
import getStats from "../util/stats";

export default function ResultData({ result }) {
  console.log(JSON.stringify(result));
  const { lesson, lessonCharCount, timeToFinish, typos } = result;

  const lessonName = getLessonNameById(lesson);
  const formattedTime = formatTime(timeToFinish);
  const stats = getStats(timeToFinish, lessonCharCount, typos);
  console.log(
    "time to finish: " +
      timeToFinish +
      " lessonCharCount: " +
      lessonCharCount +
      "typos: " +
      typos,
  );
  console.log(stats);

  const colNameClasses = "font-bold";
  const colValueClasses = "text-center";

  return (
    <div className="grid grid-cols-2 gap-4 place-content-evenly">
      <p className={colNameClasses}>Lesson</p>
      <p className={colValueClasses}>{lessonName}</p>
      <p className={colNameClasses}>Time</p>
      <p className={colValueClasses}>{formattedTime}</p>
      <p className={colNameClasses}>Speed</p>
      <p className={colValueClasses}>{stats.bpm} bpm</p>
      <p className={colNameClasses}>Accuracy</p>
      <p className={colValueClasses}>{stats.accuracy} %</p>
    </div>
  );
}
