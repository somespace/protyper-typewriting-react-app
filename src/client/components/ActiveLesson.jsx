import LessonScoreContext from "../store/LessonScoreContext";
import { useContext, useState, useEffect } from "react";
import { loadLessonText, saveLesson } from "../http";
import { getLessonNameById } from "../util/stats";
import ErrorAlert from "./ErrorAlert";
import SuccessAlert from "./SuccessAlert";
import TextPanel from "./TextPanel";
import Modal from "../UI/Modal";
import ResultDialog from "./ResultDialog";

export default function ActiveLesson() {
  const lessonScoreCtx = useContext(LessonScoreContext);

  const [isLoading, setIsLoading] = useState(false);
  const [sourceText, setSourceText] = useState([""]);
  const [lessonCharCount, setLessonCharCount] = useState(0);
  const [showResults, setShowResults] = useState(false);
  const [error, setError] = useState({ message: "" });
  const [success, setSuccess] = useState({ message: "" });

  const lesson = lessonScoreCtx.lesson;
  const timeToFinish = lessonScoreCtx.timeToFinish;
  const typos = lessonScoreCtx.typos;

  const isLessonActive = lessonScoreCtx.lesson !== "";
  const isLessonFinished = lessonScoreCtx.finished;

  useEffect(() => {
    async function loadSources() {
      setIsLoading(true);
      try {
        const sourceText = await loadLessonText(lessonScoreCtx.lesson);
        setSourceText(sourceText);
        setLessonCharCount(
          sourceText.reduce((total, line) => {
            return total + line.length;
          }, 0),
        );
      } catch (error) {
        setSuccess((prevSucc) => ({
          ...prevSucc,
          message: "",
        }));
        setError((prevErr) => ({
          ...prevErr,
          message: "The source text could not be loaded.",
        }));
      }
      setIsLoading(false);
    }

    if (isLessonActive) {
      loadSources();
    }

    if (isLessonFinished) {
      setShowResults(true);
    }
  }, [isLessonActive, isLessonFinished]);

  function handleCloseResults() {
    setShowResults(false);
    lessonScoreCtx.resetLessonStats();
  }

  const handleSaveResults = async () => {
    try {
      await saveLesson({
        lesson: lessonScoreCtx.lesson,
        timeToFinish: lessonScoreCtx.timeToFinish,
        typos: lessonScoreCtx.typos,
      });
      setSuccess((prevSucc) => ({
        ...prevSucc,
        message: "Your score was saved.",
      }));
      setError((prevErr) => ({
        ...prevErr,
        message: "",
      }));
    } catch (error) {
      setError((prevErr) => ({
        ...prevErr,
        message: error.message,
      }));
      setSuccess((prevSucc) => ({
        ...prevSucc,
        message: "",
      }));
    } finally {
      handleCloseResults();
    }
    return false;
  };

  return (
    <>
      <Modal open={showResults} onClose={handleCloseResults}>
        <ResultDialog
          lesson={lesson}
          lessonCharCount={lessonCharCount}
          timeToFinish={timeToFinish}
          typos={typos}
          onSave={handleSaveResults}
          onClose={handleCloseResults}
        />
      </Modal>
      {isLessonActive && (
        <div>
          {isLoading && <p>Loading source text...</p>}
          {!isLoading &&
            lessonScoreCtx.lesson !== "" &&
            error.message === "" && <TextPanel text={sourceText} />}
        </div>
      )}
      <ErrorAlert message={error.message} shouldBeOpen={error.message !== ""} />
      <SuccessAlert
        message={success.message}
        shouldBeOpen={success.message !== ""}
      />
    </>
  );
}
