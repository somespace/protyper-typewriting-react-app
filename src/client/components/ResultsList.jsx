import ResultsListItem from "./ResultsListItem";

export default function ResultsList({ results }) {
  return (
    <ul className="overflow-auto h-[60vh] w-[50vw]">
      {results.map((res) => (
        <li key={res.id}>
          <ResultsListItem result={res} />
        </li>
      ))}
    </ul>
  );
}
