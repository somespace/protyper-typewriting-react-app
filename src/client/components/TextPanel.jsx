import LessonScoreContext from "../store/LessonScoreContext";
import { useContext, useState, useEffect, useRef } from "react";
import { v4 as uuidv4 } from "uuid";
import CharBlock from "./CharBlock";

export default function TextPanel({ text }) {
  const lessonScoreCtx = useContext(LessonScoreContext);

  const [selectedCharIndex, setSelectedCharIndex] = useState(0);
  const [currentLineIndex, setCurrentLineIndex] = useState(0);
  const [typoCount, setTypoCount] = useState(0);
  const [timeSeconds, setTimeSeconds] = useState(0);
  const [timeIsRunning, setTimeIsRunning] = useState(true);

  const line = text[currentLineIndex];
  const [lineErrorStates, setLineErrorStates] = useState(
    Array(line.length).fill(false),
  );
  const lastLineIndex = text.length - 1;
  const lastCharIndex = line.length - 1;

  const panelRef = useRef(null);

  const focusTextPanel = () => panelRef.current.focus();

  useEffect(() => {
    if (!lessonScoreCtx.finished) {
      focusTextPanel();
    } else {
      setTimeIsRunning(false);
    }
  }, [lessonScoreCtx]);

  useEffect(() => {
    let interval;
    const ms = 1000;
    if (timeIsRunning) {
      interval = setInterval(() => {
        setTimeSeconds((prevTime) => prevTime + ms / 1000);
      }, ms);
    }

    return () => clearInterval(interval);
  }, [timeIsRunning]);

  function handleKeyDown(event) {
    console.log(event.key);
    // ignore pressing functional keys
    if (event.key.length > 1) {
      return;
    }

    // validate key press
    if (event.key !== line[selectedCharIndex]) {
      const newErrorStates = [...lineErrorStates];
      newErrorStates[selectedCharIndex] = true;
      setLineErrorStates(newErrorStates);
      setTypoCount((prevCount) => prevCount + 1);
    }

    if (selectedCharIndex === lastCharIndex) {
      setSelectedCharIndex(0);
      // show results on last character typed
      if (currentLineIndex >= lastLineIndex) {
        lessonScoreCtx.setLessonStats({
          timeToFinish: timeSeconds,
          typos: typoCount,
          finished: true,
        });
      } else {
        setCurrentLineIndex((prevLineIndex) => prevLineIndex + 1);
        setLineErrorStates((prevErrorStates) =>
          Array(text[currentLineIndex]).fill(false),
        );
      }
    } else {
      setSelectedCharIndex((prevIndex) => prevIndex + 1);
    }
  }

  return (
    <>
      {!lessonScoreCtx.finished && (
        <ul
          id="source"
          ref={panelRef}
          className="flex flex-row text-2xl select-none focus:outline-none 
      focus:bg-slate-100 rounded-lg p-5"
          tabIndex={0}
          onKeyDown={handleKeyDown}
          onMouseEnter={focusTextPanel}
        >
          {line.split("").map((char, index) => (
            <CharBlock
              key={uuidv4()}
              character={char}
              isSelected={index === selectedCharIndex}
              wasTyped={index < selectedCharIndex}
              hasError={lineErrorStates[index]}
            />
          ))}
        </ul>
      )}
    </>
  );
}
