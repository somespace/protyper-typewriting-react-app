import Alert from "../UI/Alert";

export default function ErrorAlert({ message, shouldBeOpen }) {
  return <Alert type="error" message={message} shouldBeOpen={shouldBeOpen} />;
}
