import { useContext } from "react";
import Alert from "../UI/Alert";
import LessonScoreContext from "../store/LessonScoreContext";

export default function InfoAlert() {
  const lessonScoreCtx = useContext(LessonScoreContext);

  return (
    <Alert
      type="info"
      message="Start by selecting a lesson."
      shouldBeOpen={lessonScoreCtx.lesson === ""}
    />
  );
}
