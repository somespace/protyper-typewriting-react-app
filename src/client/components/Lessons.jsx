import LessonCart from "./LessonCart.jsx";
import LessonScoreContext from "../store/LessonScoreContext.jsx";
import { useContext } from "react";

export default function Lessons() {
  const lessonScoreCtx = useContext(LessonScoreContext);

  if (lessonScoreCtx.lesson !== "") {
    return;
  }

  return (
    <div className="grid sm:grid-cols-3 gap-3 content-center sm:gap-5">
      <LessonCart
        id="basics"
        title="Basics"
        description="Basics of typewriting"
      />
      <LessonCart
        id="specchar"
        title="Special characters"
        description="All special characters used for writing code"
      />
      <LessonCart
        id="jssnippets"
        title="JavaScript snippets"
        description="Most commonly used keywords and snippets in JavaScript"
      />
    </div>
  );
}
