export default function Footer() {
  return (
    <footer
      className="flex items-center justify-center h-[10vh] 
      text-center select-none"
    >
      &#169; Martin Schneider 2024
    </footer>
  );
}
