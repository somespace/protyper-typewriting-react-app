export default function Main({ children }) {
  return (
    <>
      <main className="flex items-center justify-center px-5 sm:px-15 sm:mx-5 h-[70vh]">
        {children}
      </main>
    </>
  );
}
