import { LessonScoreContextProvider } from "../store/LessonScoreContext.jsx";
import Main from "../components/Main.jsx";
import Lessons from "../components/Lessons.jsx";
import ActiveLesson from "../components/ActiveLesson.jsx";
import InfoAlert from "../components/InfoAlert.jsx";

export default function HomePage() {
  return (
    <>
      <LessonScoreContextProvider>
        <Main>
          <Lessons />
          <ActiveLesson />
        </Main>
        <InfoAlert />
      </LessonScoreContextProvider>
    </>
  );
}
