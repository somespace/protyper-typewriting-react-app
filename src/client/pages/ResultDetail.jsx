import { useLocation } from "react-router-dom";
import { timestampToLocaleDate } from "../util/timeFormatter";
import { timestampToLocaleTime } from "../util/timeFormatter";
import Main from "../components/Main";
import ResultData from "../components/ResultData";

export default function ResultDetailPage() {
  const location = useLocation();
  const result = location.state;
  const timestamp = result.timestamp;

  const localResultDate = timestampToLocaleDate(timestamp);
  const localResultTime = timestampToLocaleTime(timestamp);

  return (
    <Main>
      <section>
        <div className="my-4 px-6 text-center">
          <h2 className="text-xl font-bold py-3">Saved Score</h2>
          <time className="text-slate-600 py-2">
            {localResultDate} - {localResultTime}
          </time>
        </div>
        <ResultData result={result} />
      </section>
    </Main>
  );
}
