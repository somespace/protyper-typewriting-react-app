import { Suspense } from "react";
import { useLoaderData, Await, defer } from "react-router-dom";
import Main from "../components/Main";
import { fetchResults } from "../http";
import ResultsList from "../components/ResultsList";

export default function ResultsPage() {
  const { results } = useLoaderData();

  return (
    <>
      <Main>
        <Suspense fallback={<p style={{ textAlign: "center" }}>Loading...</p>}>
          <Await resolve={results}>
            {(loadedResults) => <ResultsList results={loadedResults} />}
          </Await>
        </Suspense>
      </Main>
    </>
  );
}

export async function loadResults() {
  const results = await fetchResults();
  return results;
}

export function loader() {
  return defer({
    results: loadResults(),
  });
}
