import Footer from "../components/Footer";
import Header from "../components/Header";
import Main from "../components/Main";
import MainNavigation from "../components/MainNavigation";

export default function ErrorPage() {
  return (
    <>
      <Header />
      <MainNavigation />
      <Main>
        <div className="text-center">
          <h1 className="text-slate-400 text-9xl select-none mb-14">:(</h1>
          <h2 className="font-bold mb-2 text-2xl">Error 404</h2>
          <p>Page was not found!</p>
        </div>
      </Main>
      <Footer />
    </>
  );
}
