import { Outlet } from "react-router-dom";

import Header from "../components/Header.jsx";
import MainNavigation from "../components/MainNavigation";
import Footer from "../components/Footer.jsx";

export default function RootLayout() {
  return (
    <>
      <Header />
      <MainNavigation />
      <Outlet />
      <Footer />
    </>
  );
}
