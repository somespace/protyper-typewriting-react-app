import { useRef, useEffect } from "react";
import { createPortal } from "react-dom";

export default function Modal({ children, open, onClose }) {
  const dialog = useRef(null);

  useEffect(() => {
    const modal = dialog.current;

    if (open) {
      modal.showModal();
    }

    return () => modal.close();
  }, [open]);

  return createPortal(
    <>
      <dialog
        ref={dialog}
        className="w-full md:w-2/6 backdrop:bg-slate-900/90 bg-slate-100 text-slate-700 p-4 
      rounded-md shadow-md"
        style={{ cursor: "default" }}
        onClose={onClose}
      >
        {children}
      </dialog>
    </>,
    document.getElementById("modal"),
  );
}
