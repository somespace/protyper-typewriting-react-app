import { useEffect, useState } from "react";
import { createPortal } from "react-dom";

export default function Alert({ type, message, shouldBeOpen = true }) {
  const [isOpen, setIsOpen] = useState(true);
  const [timeIsRunning, setTimeIsRunning] = useState(true);

  useEffect(() => {
    let timer;
    const timeOut = 5000;

    if (timeIsRunning && shouldBeOpen) {
      timer = setTimeout(() => {
        setIsOpen(false);
        setTimeIsRunning(false);
      }, timeOut);
    }

    return () => clearTimeout(timer);
  }, [timeIsRunning, shouldBeOpen]);

  let alertClasses =
    "fixed bottom-[10vh] left-5 right-5 p-5 sm:w-auto text-white sm:left-auto rounded-lg shadow-lg";

  if (type === "error") {
    alertClasses += " bg-slate-950";
  } else {
    alertClasses += " bg-slate-700";
  }

  function handleCloseAlert() {
    setIsOpen(false);
  }

  if (!isOpen || !shouldBeOpen) {
    return;
  }

  return createPortal(
    <div className={alertClasses}>
      <span
        className="ml-[15px] text-white font-bold float-right text-[22px] leading-5"
        onClick={handleCloseAlert}
      >
        &times;
      </span>
      <strong className="capitalize">{type}!&nbsp;&nbsp;</strong>
      {message}
    </div>,
    document.getElementById("alert"),
  );
}
