import { RouterProvider, createBrowserRouter } from "react-router-dom";
import RootLayout from "./pages/RootLayout.jsx";
import HomePage from "./pages/Home.jsx";
import ResultDetailPage from "./pages/ResultDetail.jsx";
import ResultsPage, { loader as resultsLoader } from "./pages/Results.jsx";
import ErrorPage from "./pages/Error.jsx";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      {
        index: true,
        element: <HomePage />,
      },
      {
        path: "/results",
        element: <ResultsPage />,
        loader: resultsLoader,
      },
      {
        path: "/results/:resultId",
        element: <ResultDetailPage />,
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
